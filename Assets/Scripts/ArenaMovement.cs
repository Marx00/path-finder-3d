﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaMovement : MonoBehaviour {

    public float max_tilt = 10f;
    public float time_to_tilt = 1f;
    public float current_x_tilt;
    public float current_z_tilt;

	// Use this for initialization
	void Start () {
        current_x_tilt = 0;
        current_z_tilt = 0;
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow)) { }

        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            current_x_tilt += -10 * Time.deltaTime;
            if (Mathf.Abs(current_x_tilt) > max_tilt)
                current_x_tilt = -max_tilt;
        }

        else if (Input.GetKey(KeyCode.RightArrow))
        {
            current_x_tilt += 10 * Time.deltaTime;
            if (Mathf.Abs(current_x_tilt) > max_tilt)
                current_x_tilt = max_tilt;
        }

        else
        {
            if (current_x_tilt > 0)
            {
                current_x_tilt += -10 * Time.deltaTime;
                if (current_x_tilt < 0)
                    current_x_tilt = 0;
            }

            if (current_x_tilt < 0)
            {
                current_x_tilt += 10 * Time.deltaTime;
                if (current_x_tilt > 0)
                    current_x_tilt = 0;
            }
        }

        if (Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.UpArrow)) { }

        else if (Input.GetKey(KeyCode.DownArrow))
        {
            current_z_tilt += -10 * Time.deltaTime;
            if (Mathf.Abs(current_z_tilt) > max_tilt)
                current_z_tilt = -max_tilt;
        }

        else if (Input.GetKey(KeyCode.UpArrow))
        {
            current_z_tilt += 10 * Time.deltaTime;
            if (Mathf.Abs(current_z_tilt) > max_tilt)
                current_z_tilt = max_tilt;
        }

        

        else
        {
            if (current_z_tilt > 0)
            {
                current_z_tilt += -10 * Time.deltaTime;
                if (current_z_tilt < 0)
                    current_z_tilt = 0;
            }

            if (current_z_tilt < 0)
            {
                current_z_tilt += 10 * Time.deltaTime;
                if (current_z_tilt > 0)
                    current_z_tilt = 0;
            }
        }

        transform.rotation = Quaternion.Euler(new Vector3(current_x_tilt, 0, current_z_tilt));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            collision.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 1, 0));
        }
    }
}
