﻿using UnityEngine;
using System.Collections;

public class QuitOnClick : MonoBehaviour
{
    public AudioClip chime;
    

    public void Quit()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = chime;
        audio.Play();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit ();
#endif
    }

}
