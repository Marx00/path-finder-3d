﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava_Controller : MonoBehaviour {

    public PlayerBall_controller player;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float lerp = Mathf.PingPong(Time.time, 1);
        gameObject.GetComponent<Renderer>().material.color = new Color(1f, lerp, lerp, 1f);
	}

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            player.is_heated = true;
            if (!player.is_metal)
                player.restart();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Ball")
            player.is_heated = false;
    }

}
