﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvisibilityBuff : MonoBehaviour {

    public PlayerBall_controller player;
    public Renderer Player_Ball;
    public Collider[] Walls;
    public Text InvisTimer;
    public AudioClip chime;
    private static float start_time;
    private static float max_time;
    private static float time_left;
	
    // Use this for initialization
	void Start () {
        time_left = 0;
        start_time = 0;
        max_time = 50;
	}
	
	// Update is called once per frame
	void Update () {
		if (player.is_invis)
        {
            start_time += Time.deltaTime;
            time_left = max_time - start_time + 1f;
            InvisTimer.text = "Invisibility " + ((int)time_left).ToString();
            if (start_time >= max_time)
            {
                TogglePassThrough();
 //               Player_Ball.material.color = new Color(1.0f, 1.0f, 1.0f, 1f);
                player.is_invis = false;
                start_time = 0f;
                InvisTimer.enabled = false;
            }
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.clip = chime;
            audio.Play();
            start_time = 0f;
            time_left = max_time - start_time + 1f;

            if (!player.is_invis)
            {
                InvisTimer.text = "Invisibility " + time_left.ToString();
                InvisTimer.enabled = true;
 //               Player_Ball.material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
                TogglePassThrough();
            }
            player.is_invis = true;
        }
    }

    private void TogglePassThrough()
    {
        if (player.is_invis)
        {
            foreach (Collider wall in Walls)
            {
                wall.isTrigger = false;
            }
        }

        else if (!player.is_invis)
        {
            foreach (Collider wall in Walls)
            {
                wall.isTrigger = true;
            }
        }
    }
}
