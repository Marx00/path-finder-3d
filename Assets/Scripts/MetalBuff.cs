﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MetalBuff : MonoBehaviour {

    public PlayerBall_controller player;
    public Renderer Player_Ball;
    public Text MetalTimer;
    public Material Base_mat;
    public Material Metal_mat;
    public AudioClip chime;
    private static float start_time;
    private static float max_time;
    private static float time_left;

    // Use this for initialization
    void Start () {
        time_left = 0;
        start_time = 0;
        max_time = 50;
    }
	
	// Update is called once per frame
	void Update () {
        if (player.is_metal)
        {
            start_time += Time.deltaTime;
            time_left = max_time - start_time + 1f;
            MetalTimer.text = "Metallic " + ((int)time_left).ToString();
            if (start_time >= max_time)
            {
//                Player_Ball.material = Base_mat;
                player.is_metal = false;
                start_time = 0f;
                MetalTimer.enabled = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.clip = chime;
            audio.Play();
            start_time = 0f;
            time_left = max_time - start_time + 1f;

            if (!player.is_metal)
            {
                MetalTimer.text = "Metallic " + time_left.ToString();
                MetalTimer.enabled = true;
 //               Player_Ball.material = Metal_mat;
            }
            player.is_metal = true;
        }
    }
}
