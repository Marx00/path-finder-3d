﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadOnClick : MonoBehaviour {

    public AudioClip chime;

    public void LoadByIndex(int sceneIndex)
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = chime;
        audio.Play();
        SceneManager.LoadScene(sceneIndex);
    }

}
