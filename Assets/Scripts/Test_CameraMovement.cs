﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_CameraMovement : MonoBehaviour {

    private float _Yangle = 0;
    private float _Xangle = 0;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            _Yangle += 5;
            transform.rotation = Quaternion.Euler(_Xangle, _Yangle, 0);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            _Yangle -= 5;
            transform.rotation = Quaternion.Euler(_Xangle, _Yangle, 0);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            _Xangle -= 5;
            transform.rotation = Quaternion.Euler(_Xangle, _Yangle, 0);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            _Xangle += 5;
            transform.rotation = Quaternion.Euler(_Xangle, _Yangle, 0);
        }
    }
}
