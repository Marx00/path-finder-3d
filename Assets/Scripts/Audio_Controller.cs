﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/*
 * This script handles the music that is played for each scene.
 */
public class Audio_Controller : Singleton<Audio_Controller> {

    public AudioSource[] back_music;    //Array that holds the 3 songs that will play for each scene
    public int current_scene = 0;       //Initialization to beginning seen, triggeres the if statement below if on another scene

    //By default, plays the opening scene's music, but if the scene index is different, then it will change to that one.
	void Update () {
        if (current_scene != SceneManager.GetActiveScene().buildIndex) {
            back_music[current_scene].Stop(); //Turn off the previous scene's music
            current_scene = SceneManager.GetActiveScene().buildIndex;
            back_music[current_scene].Play(); //Turn on new scene's music
        }
    }
}
