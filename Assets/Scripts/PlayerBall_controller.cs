﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBall_controller : MonoBehaviour {

    public Material base_mat;
    public Material metal_mat;
    public Material heated_mat;
    public Transform camera;

    public bool is_metal;
    public bool is_invis;
    public bool is_heated;

	// Use this for initialization
	void Start () {
        gameObject.GetComponent<Rigidbody>().AddForce(10 * Vector3.forward);
        is_metal = false;
        is_invis = false;
        is_heated = false;
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        if (Input.GetKeyDown(KeyCode.R))
            SceneManager.LoadScene(1, LoadSceneMode.Single);

        Renderer player = gameObject.GetComponent<Renderer>();

        camera.position = gameObject.GetComponent<Transform>().position;
        camera.Translate(0, 0, -5);

        if (!is_metal && is_invis && !is_heated)
        {
            player.material = base_mat;
            player.material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
        }

        else if (is_metal && !is_invis && !is_heated)
        {
            player.material = metal_mat;
            player.material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        }

        else if (is_metal && !is_invis && is_heated)
        {
            player.material = heated_mat;
            player.material.color = new Color(1.0f, 1.0f, 1.0f, 1f);
        }

        else if (is_metal && is_invis && !is_heated)
        {
            player.material = metal_mat;
            player.material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
        }

        else if (is_metal && is_invis && is_heated)
        {
            player.material = heated_mat;
            player.material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
        }

        else
        {
            player.material = base_mat;
            player.material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Win")
            victory();
    }

    public void restart()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void victory()
    {
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }


}
