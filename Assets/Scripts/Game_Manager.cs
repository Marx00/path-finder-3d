﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//This script will act as an interface for reloading, loading, and exiting scenes.
public class Game_Manager : Singleton<Game_Manager> {

    public bool[] can_interrupt_game = { false, true, false };  //Whether or not the scene (represented by index) can be interrupted by the player
    public int current_scene;

	// Get our current scenes index
	void Start () {
        current_scene = SceneManager.GetActiveScene().buildIndex;
    }
	
	// Check if the player is allowed to interrupt in the current scene
    // If so, then they can reload and quit scenes
    // Otherwise, lock the player out of these commands
	void Update () {
        if (can_interrupt_game[current_scene])
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                SceneManager.LoadScene(current_scene, LoadSceneMode.Single);
                Time.timeScale = 1;
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
    }

    //The only method that is allowed to load new scenes.
    //If objects not of this class wish to load, they must have access to this Game_Manager
    public void Loadlevel(int scene)
    {
        if (scene == 3)
            Application.Quit();
        else
            SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }
}
